FROM ubuntu:16.04

MAINTAINER Morten Bornø Jensen <mboj@create.aau.dk>

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
      build-essential \
      git \
      lsb-release \
      sudo \
      udev \
      usbutils \
      wget \
      nano \
      vim \
      curl \
    && apt-get clean all




RUN wget https://ncs-forum-uploads.s3.amazonaws.com/ncsdk/ncsdk-02_05_00_02-full/ncsdk-2.05.00.02.tar.gz
RUN tar zxvf ncsdk-2.05.00.02.tar.gz
RUN apt-get clean
WORKDIR ncsdk-2.05.00.02
RUN make install
RUN export PYTHONPATH="${PYTHONPATH}:/opt/movidius/caffe/python"
RUN pip3 install imutils opencv-python opencv-contrib-python
#RUN make examples

# Replace 1000 with your user / group id
#RUN export uid=1000 gid=1000 && \
#    mkdir -p /home/developer && \
#    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
#    echo "developer:x:${uid}:" >> /etc/group && \
#    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
#    chmod 0440 /etc/sudoers.d/developer && \
#    chown ${uid}:${gid} -R /home/developer

#USER developer
#ENV HOME /home/developer

WORKDIR /home
RUN git clone -b ncsdk2 https://github.com/movidius/ncappzoo.git

WORKDIR /home/ncappzoo/tensorflow/tf_src
RUN make
ENV TF_SRC_PATH="/home/ncappzoo/tensorflow/tf_src/tensorflow"


WORKDIR /home/ncappzoo/tensorflow/tf_models
RUN make
ENV TF_MODELS_PATH="/home/ncappzoo/tensorflow/tf_models/models"
WORKDIR /host


# Platform Evaluation for Intel Movidius stick
This repo contains a dockerfile and belonging scripts to build the dockerfile, start the dockerimages, and if required, also clean up the builded docker images. Furthermore, the repo contains a code directory which contain a handler python script for performing FPS evaluation using Mobilenet_v1 on 1-to-n Intel Movidius Neural AI Compute Stick. Below is a walkthrough of how to get things up and running.

## Pre-requisites
- Docker
- 1 or more Intel Movidius Neural AI Compute Stick

## 1. Build the dockerfile
The first step is quite straight forward. You need to built the actual dockerfile. The dockerfiles is based upon a ubuntu 16.04 and will automatically installed the ncsdk-2.05.00.02
To build the docker fil use the execute the buildDocker.sh script


```
sh buildDockerScript.sh
```
As you are fetching a clean Ubuntu 16.04 docker image, you need to build the aforementioned package and a few extra things. So it will take around 10-15 minutes to build. In the meantime you can enjoy this [movie](https://www.youtube.com/watch?v=6j53Du7hWxI).

## 2. Start the docker image
Congratz! You are now one step closer to executing your first Intel Movidius benchmark test using Mobilenet_v1 and inception_v1, v2, v3 and v4.
The following step is quite straight forward as well. Just execute the startDocker.sh and you are all set!

```
sh startDocker.sh 
```
## 3. Run the mobilenet_v1 on NCS
Now we have the entire environment up and running. The starting point of the docker is the host directory, which is fact the local "code" directory cloned from github repo. If you take a look at the startDocker.sh you will discover, that you have just mounted the local "code" directory to your docker environment. So the stuff you work on here will be saved on your local computer.
Okay - enough with the chit chat - head to the ncsBenchmarkHandler folder and utilize the run function in the makefile:
```
(cd ncsBenchmarkHandler && make run)
```
Hopefully you will see that the checkpoint file is found and converted to the movidius format. 

## Results
Currently the makefile supports Mobilenet_V1, inception_v1, inception_v2, inception_v3, and inception_v4. The results are seen below.


| Model                           | Batchsize | Imgsize | Intel NCS - #1 | Intel NCS - #2 | Intel NCS - #3 | Intel NCS - #4 | Intel NCS - #5 |
|---------------------------------|----------:|--------:|---------------:|---------------:|---------------:|---------------:|---------------:|
| mobilenet_v1_1.0_224_2017_06_14 |         1 |     224 |      18.10 FPS |      36.46 FPS |      51.85 FPS |      67.54 FPS |      80.15 FPS |
| inception_v1_2016_08_28         |         1 |     224 |       9.60 FPS |      19.20 FPS |      26.93 FPS |      35.87 FPS |      45.46 FPS |
| inception_v2_2016_08_28         |         1 |     224 |       7.35 FPS |      14.76 FPS |      20.90 FPS |      28.12 FPS |      34.91 FPS |
| inception_v3_2016_08_28         |         1 |     224 |       5.15 FPS |      10.31 FPS |      15.05 FPS |      19.54 FPS |      24.88 FPS |
| inception_v4_2016_09_09         |         1 |     299 |       1.52 FPS |       2.90 FPS |       4.00 FPS |       5.32 FPS |       5.89 FPS |


Be aware of the inception_v4, I did that test with an imgsize of 299. This should be redone with 224 :)

All experiments are executed on Lenovo Thinkpad T?? with an Intel Core i7-7820HQ CPU @ 2.90GHz x 8  and 16gb memory as a host.

## Addtional
Please be careful with the hardCleanup.sh... It will remove every docker image you have. You have been warned! Wupti - and all the work is gone. Only use it if you are entirely confident about it.

## Usefull Links
- https://github.com/pkdogcom/opencv-docker
